const cloud = require('wx-server-sdk')

cloud.init()
const db = cloud.database(),_=db.command
exports.main = async (event, context) => {
	const wxContext = cloud.getWXContext()
	let staffs = await db.collection('staff').where({
		_openid:wxContext.OPENID,
		group_id:_.in([1, 2])
	}).skip(event.skip).limit(event.limit).get()
	staffs = staffs.data
	let shops = []
	for(let i=0;i<staffs.length;i++){
		let shop = await db.collection('shop').doc(staffs[i].shop_id).field({
			status:true,
			name:true,
			_id:true
		}).get()
		if(shop.data.status==2||staffs[i].group_id==1){
			shops.push(shop.data)
		}
	}
	return {
		code:200,
		data:shops
	}
}