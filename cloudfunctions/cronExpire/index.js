// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()
const db = cloud.database()
const _ = db.command

function logs(result) {
	return db.collection('crontab').add({
		data: {
			create_time: new Date,
			result: result,
			identifier: 'expire'
		}
	})
}
exports.main = async (event, context) => {
	let date = new Date()
	date.setDate(date.getDate()-1)
	let result = await db.collection('appoint').where({
		status:2,
		appoint_date:_.lt(date.getTime())
	}).update({
		data:{
			status:5
		}
	})
	return logs(result)
	
}