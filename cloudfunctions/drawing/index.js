// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init({
  env: "order-ys8h4"
})

const db =cloud.database()
// 云函数入口函数
exports.main = async (event, context) => {
  

  //获取该预约项目下预约时间段id

  const timeline=await db.collection('timeline').where({
    daily_id:event._id
  }).get()
  
  let timelines=timeline.data
  //1.循环修改每个时间段的预约记录
  for (let i = 0; i < timelines.length;i++){
    //2.随机抽取指定数量预约记录并修改预约成功用户状态
    console.log(timelines[i]._id)
    console.log(timelines[i].nums_max)
      const userInfos = await db.collection('appoint')
      .aggregate()
        .match({ 
          timeline_id: timelines[i]._id,
          status:1
        })
        .sample({
          size: timelines[i].nums_max
        })
      .end()
      //修改预约成功用户状态
      const userInfo=userInfos.list
    console.log(userInfos)
    console.log(userInfo)
      for(let j =0;j<userInfo.length;j++){
        await db.collection('appoint').where({
          _id: userInfo[j]._id
        }).update({
          data:{
            status: 2
          },
          success:function(res){
            console.log(res.data)
          }
        })
      }
    //将等待审核的预约记录修改至审核失败
    try {
      const userInfo = await db.collection('appoint').where({
        timeline_id: timelines[i]._id,
        status:1
      }).update({
        data: {
          status: 3
        },
        success: function (res) {
          console.log(res.data)
        },
      })
    } catch (e) {
      console.log(e)
      return {
        code: 201,//状态码，200为正常
        msg: "获取预约记录失败 ",//若启用了默认方法并有此参数则会调用wx.showToast,
        url: "",//若启用了默认方法并有此参数则会调用wx.redirectTo,
        data: {},//返回数据集，格式自定义
      }
    }
  }
    return{
      code: 200,//状态码，200为正常
      msg: "抽签完成",//若启用了默认方法并有此参数则会调用wx.showToast,
      url: "",//若启用了默认方法并有此参数则会调用wx.redirectTo,
      data: {},//返回数据集，格式自定义
    }
}