const app = getApp()
Page({

	data: {
		statusAppointTitle:app.globalData.statusAppointTitle,
		list:[],
		result:''
	},

	onLoad: function (options) {
		this.getData()
	},
	getData(){
		app.cloud({
			name:'userAppoints',
			data:{
				skip:this.data.list.length,
			},
			loading:'加载中',
			def:true,
			success:res=>{
				wx.stopPullDownRefresh()
				if(res.data.length==0){
					if(this.data.list.length==0) return this.setData({result:'当前还没有您的预约记录'})
					return wx.showToast({
					  title: '没有更多记录了',
					  icon:'none'
					})
				}
				let list = this.data.list.concat(res.data)
				this.setData({list})
			}
		})
	},
	onPullDownRefresh(){
		this.setData({list:[],result:''})
		this.getData()
	},
	onReachBottom(){
		this.getData(0)
	}
})