const app = getApp()
Page({

	data: {

		result: '页面加载中',
		type: 1
	},
	onLoad: function (options) {
		this.setData(options)
		let db = app.db
		db.collection('timeline').doc(options._id).field({
			result_status: false
		}).get({
			success: res => {
				this.setData(res.data)

				//先找shop
				db.collection('shop').doc(this.data.shop_id).field({
					address: true,
					location: true,
					_id: false,
					is_visit: true,
					status: true,
					name: true
				}).get({
					success: res => {
						if (!res.data.is_visit || res.data.status != 2) {
							return this.setData({
								result: '门店不存在'
							})
						}
						this.setData({
							shop: res.data
						})
						db.collection('product').doc(this.data.product_id).field({
							name: true,
							_id: false,
							price: true,
							to_home: true,
							to_shop: true,
							code_id: true,
						}).get({
							success: res => {
								wx.setNavigationBarTitle({
									title: '预约' + res.data.name,
								})
								this.setData({
									product: res.data
								})
								if (!this.data.product.to_shop) {
									this.setData({
										type: 'to_home'
									})
								} else {
									this.setData({
										type: 'to_shop'
									})
								}
								db.collection('daily').doc(this.data.daily_id).field({
									_id: false,
									product_id: false,
									result_status: false,
									shop_id: false
								}).get({
									success: res => {
										if (!res.data.is_allow) {
											return this.setData({
												result: '当前不可预约'
											})
										}
										let date = (new Date()).getTime()
										if (date < res.data.time_begin) {
											return this.setData({
												result: app.getDatetime(res.data.time_begin) + '开始预约'
											})
										}
										if (date > res.data.time_end) {
											return this.setData({
												result: app.getDatetime(res.data.time_end) + '已结束预约'
											})
										}
										res.data.time_result = app.getDatetime(res.data.time_result)
										this.setData({
											daily: res.data
										})
										//校验人数
										if (this.data.nums_limit || this.data.daily.type == 2) {
											if (this.data.nums_cnt == this.data.nums_max) {
												return this.setData({
													result: '预约人数已满'
												})
											}
										}
										this.setData({
											result: ''
										})
									},
									fail: res => {
										this.setData({
											result: '无法预约'
										})
									}
								})
							},
							fail: res => {
								this.setData({
									result: '无法预约'
								})
							}
						})
					},
					fail: res => {
						this.setData({
							result: '无法预约'
						})
					}
				})
			},
			fail: res => {
				this.setData({
					result: '无法预约'
				})
			}
		})
	},
	changeValue(e) {
		this.setData({
			[e.currentTarget.dataset.key]: e.detail.value
		})
	},
	openLocation() {
		wx.openLocation({
			latitude: this.data.shop.location.latitude,
			longitude: this.data.shop.location.longitude,
			address: this.data.shop.address,
			name: this.data.shop.name
		})
	},
	chooseAddress() {
		wx.chooseAddress({
			success: (res) => {
				console.log(res)
				delete res.errMsg
				this.setData({
					address: res
				})
			},
		})
	},
	onSubmit(e) {
		let data = e.detail.value
		if (!data.name || data.name.length == 0) {
			return wx.showToast({
				title: '请输入姓名',
				icon: 'none'
			})
		}
		if (!data.idcard || data.idcard.length == 0) {
			return wx.showToast({
				title: '请输入身份证号',
				icon: 'none'
			})
		}
		if (!this.checkIDCard(data.idcard)) {
			return wx.showToast({
				title: '身份证号格式不正确',
				icon: 'none'
			})
		}
		if (!data.phone || data.phone.length == 0) {
			return wx.showToast({
				title: '请输入联系电话',
				icon: 'none'
			})
		}
		if (!this.data.product.to_home) {
			data.type = 'to_shop'
		} else if (!this.data.product.to_shop) {
			data.type = 'to_home'
		} else {
			data.type = this.data.type
			if (!data.type) {
				return wx.showToast({
					title: '请选择领取方式',
					icon: 'none'
				})
			}
		}
		if (data.type == 'to_home') {
			if (!this.data.address) {
				return wx.showToast({
					title: '请选择地址',
					icon: 'none'
				})
			}
			data.address = this.data.address
		}
		data._id = this.data._id
		app.cloud({
			name: 'appoint',
			data,
			loading: '预约提交中',
			def: true,
			success: res => {
				wx.showModal({
					title: res.msg,
					content: '点击确定跳转至预约详情',
					success: rs => {
						if (rs.confirm) {
							wx.navigateTo({
								url: '/pages/userAppoint/index?_id=' + res.data,
							})
						}
					}
				})
			}
		})
	},
	// 函数参数必须是字符串，因为二代身份证号码是十八位，而在javascript中，十八位的数值会超出计算范围，造成不精确的结果，导致最后两位和计算的值不一致，从而该函数出现错误。
	// 详情查看javascript的数值范围
	checkIDCard(idcode) {
		// 加权因子
		var weight_factor = [7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2];
		// 校验码
		var check_code = ['1', '0', 'X', '9', '8', '7', '6', '5', '4', '3', '2'];

		var code = idcode + "";
		var last = idcode[17]; //最后一位

		var seventeen = code.substring(0, 17);

		// ISO 7064:1983.MOD 11-2
		// 判断最后一位校验码是否正确
		var arr = seventeen.split("");
		var len = arr.length;
		var num = 0;
		for (var i = 0; i < len; i++) {
			num = num + arr[i] * weight_factor[i];
		}

		// 获取余数
		var resisue = num % 11;
		var last_no = check_code[resisue];

		// 格式的正则
		// 正则思路
		/*
		第一位不可能是0
		第二位到第六位可以是0-9
		第七位到第十位是年份，所以七八位为19或者20
		十一位和十二位是月份，这两位是01-12之间的数值
		十三位和十四位是日期，是从01-31之间的数值
		十五，十六，十七都是数字0-9
		十八位可能是数字0-9，也可能是X
		*/
		var idcard_patter = /^[1-9][0-9]{5}([1][9][0-9]{2}|[2][0][0|1][0-9])([0][1-9]|[1][0|1|2])([0][1-9]|[1|2][0-9]|[3][0|1])[0-9]{3}([0-9]|[X])$/;

		// 判断格式是否正确
		var format = idcard_patter.test(idcode);

		// 返回验证结果，校验码和格式同时正确才算是合法的身份证号码
		return last === last_no && format ? true : false;
	}

})