const app = getApp()
Page({

	data: {
		result:'页面加载中'
	},

	onLoad: function (options) {
		app.db.collection('help').doc(options._id).get({
			success: res => {
				wx.setNavigationBarTitle({
				  title: res.data.title,
				})
				res.data.result = ''
				this.setData(res.data)
			},
			fail: res => {
				this.setData({result:'内容不存在'})
				console.error(res)
			}
		})
	},
	onShareAppMessage: function () {

	}
})