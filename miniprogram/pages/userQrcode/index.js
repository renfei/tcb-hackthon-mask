import QRCode from '../../utils/weapp-qrcode.js'
const base64 = require('../../utils/base64.js')

const app = getApp()
Page({

	data: {

	},

	onLoad: function (options) {
		let that = this
		app.userInfo(function(res){
			console.log(res)
			that.setData({
				qrcode_w:app.globalData.qrcode_w
			})
			new QRCode('canvas', {
				text: base64.encode('user|'+res._openid),
				padding: 12,
				width: that.data.qrcode_w,
				height: that.data.qrcode_w,
				correctLevel: QRCode.CorrectLevel.H,
				callback: (res) => {
					// 生成二维码的临时文件
					that.setData({
						qrcode:res.path
					})
				}
			})
		})
	},
	save(){
		wx.saveImageToPhotosAlbum({
		  filePath: this.data.qrcode,
		})
	},
	showQrcode(){
		wx.previewImage({
		  urls: [this.data.qrcode],
		})
	}
})