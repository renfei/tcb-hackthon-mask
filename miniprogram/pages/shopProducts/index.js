const app = getApp()
Page({
	data: {
		page: 1,
		list: []
	},
	onLoad: function (options) {
		this.setData(options)
		this.getData(1)
	},
	getData(page) {
		wx.showLoading({
			title: '加载中',
			mask: true
		})
		app.db.collection('product').where({
			shop_id: this.data.shop_id
		}).skip((page - 1) * 15).limit(15).field({
			name: true,
			_id: true,
			price: true
		}).orderBy('create_time', 'desc').get().then(res => {
			wx.stopPullDownRefresh()
			wx.hideLoading()
			if (res.data.length == 0) {
				if (page > 1) wx.showToast({
					title: '没有更多数据了',
					icon: 'none'
				})
				return
			}
			let list = this.data.list
			list = list.concat(res.data)
			this.setData({
				list,
				page
			})
		})
	},
	onReachBottom() {
		this.getData(this.data.page + 1)
	},
	onPullDownRefresh() {
		this.setData({
			list: []
		})
		this.getData(1)
	}
})