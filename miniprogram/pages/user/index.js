const app = getApp()
Page({
	data: {
		StatusBar: app.globalData.StatusBar,
		CustomBar: app.globalData.CustomBar,
		Custom: app.globalData.Custom,

	},
	onGetUserInfo(e) {
		if (e.detail.errMsg != 'getUserInfo:ok') {
			return wx.showToast({
				title: '您需要授权后才可使用本小程序',
				icon: 'none'
			})
		}
		let userInfo = e.detail.userInfo,
			that = this

		app.login(userInfo, function (res) {
			that.setData({
				userInfo: res
			})
		})
	},
	onShow: function () {
		let that = this
		app.userInfo(function (res) {
			that.setData({
				userInfo: res
			})
		})
	},
	loginToPage(e) {
		if (!this.data.userInfo) {
			return wx.showToast({
				title: '请先登录',
				icon: 'none'
			})
		}
		wx.navigateTo({
			url: e.currentTarget.dataset.url,
		})
	},
	toPage(e) {
		wx.navigateTo({
			url: e.currentTarget.dataset.url,
		})
	},
	shareWxcode() {
		wx.previewImage({
		  urls: ['https://qnimg.dqzhiyu.com/mask_wxacode.jpg'],
		  success:res=>{
			  wx.showToast({
				title: '请保存图片',
				icon:'none'
			  })
		  }
		})
	},
	onShareAppMessage: function () {
		return {
			title: '在线预约口罩等物资',
			path: '/pages/index/index',
			imageUrl: 'https://qnimg.dqzhiyu.com/mask_share.jpg',
		}
	}
})