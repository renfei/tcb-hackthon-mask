import QRCode from '../../utils/weapp-qrcode.js'
const base64 = require('../../utils/base64.js')

const app = getApp()
Page({

	data: {
		statusAppointTitle:app.globalData.statusAppointTitle,
	},

	onLoad: function (options) {
		if(!options._id){
			return wx.showToast({
			  title: '缺少参数',
			  icon:'none'
			})
		}
		app.db.collection('appoint').doc(options._id).field({
			appoint_date:false,
		}).get({
			success:res=>{
				res.data.create_time = app.getDatetime(res.data.create_time)
				if(res.data.use_time)res.data.use_time = app.getDatetime(res.data.use_time)
				if(res.data.cancel_time)res.data.cancel_time = app.getDatetime(res.data.cancel_time)
				this.setData(res.data)
				this.setData({
					qrcode_w:app.globalData.qrcode_w
				})
				new QRCode('canvas', {
					text: base64.encode("appoint|"+options._id),
					padding: 12,
					width: this.data.qrcode_w,
					height: this.data.qrcode_w,
					correctLevel: QRCode.CorrectLevel.H,
					callback: (res) => {
						// 生成二维码的临时文件
						this.setData({
							qrcode:res.path
						})
					}
				})
				//获取shop
				app.db.collection('shop').doc(this.data.shop_id).field({
					name:true,
				}).get({
					success:res=>{
						this.setData({shop:res.data})
					}
				})
				//获取product
				app.db.collection('product').doc(this.data.product_id).field({
					name:true,
				}).get({
					success:res=>{
						this.setData({product:res.data})
					}
				})
				//获取timeline
				app.db.collection('timeline').doc(this.data.timeline_id).field({
					time_begin:true,
					time_end:true
				}).get({
					success:res=>{
						this.setData({timeline:res.data})
					}
				})
				//获取daily
				app.db.collection('daily').doc(this.data.daily_id).field({
					date:true,
					time_result:true
				}).get({
					success:res=>{
						res.data.time_result = app.getDatetime(res.data.time_result)
						this.setData({daily:res.data})
					}
				})
				
			},
			fail:res=>{
				wx.showToast({
				  title: '预约记录不存在',
				  icon:'none'
				})
				console.error(res)
			}
		})
		
	},
	save(){
		wx.saveImageToPhotosAlbum({
		  filePath: this.data.qrcode,
		})
	},
	showQrcode(){
		wx.previewImage({
		  urls: [this.data.qrcode],
		})
	}
})