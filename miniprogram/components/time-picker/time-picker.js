const leftPad0 = function (v, n) {
	if (!v) {
		v = "";
	}
	let prefix = "";
	for (let i = 0; i < n; i++) {
		prefix += "0";
	}
	return (prefix + v).substr(-n);
};
const stringToDate = function (str) {
	str = str.replace(/-/g, "/");
	return new Date(str);
};

const hours = [];
for (var i = 0; i < 24; i++) {
	hours.push(leftPad0(i, 2) + "时");
}
const minutes = [];
for (var i = 0; i < 60; i++) {
	minutes.push(leftPad0(i, 2) + "分");
}
const seconds = [];
for (var i = 0; i < 60; i++) {
	seconds.push(leftPad0(i, 2) + "秒");
}
Component({
	properties: {
		value: String,
	},

	/**
	 * 组件的初始数据
	 */
	data: {
		valueArray: [0, 0, 0],
		rangeValues: [
			hours,
			minutes,
			seconds
		],
	},
	observers: {
		value: function (v) {
			let timecolumn = v.split(':')
			this.setData({
				valueArray:[parseInt(timecolumn[0]),parseInt(timecolumn[1]),parseInt(timecolumn[2])]
			})
		},
	},
	
	methods: {

		handleCancel(e) {
			this.setData({
				valueArray: this.data.valueArray
			})
		},

		handleValueChange(e) {
			let dateArr = [];
			for (let i in e.detail.value) {
				let v = this.data.rangeValues[i][e.detail.value[i]];
				dateArr.push(v.toString().substr(0, v.length - 1))
			}
			let dateString = dateArr[0] + ":" + dateArr[1] + ':' + dateArr[2];
			this.triggerEvent('change', {
				value:dateString
			})
		}
	}
})